package ru.t1.karimov.tm.repository;

import org.junit.FixMethodOrder;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SessionRepositoryTest extends AbstractSchemeTest {

/*
    private static final int NUMBER_OF_ENTRIES = 10;

    private static final int HALF_NUMBER_OF_ENTRIES = NUMBER_OF_ENTRIES / 2;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @Nullable
    private static EntityManager entityManager = null;

    @NotNull
    private static List<SessionDto> sessionList;

    @NotNull
    private static ISessionDtoRepository sessionRepository;

    @Nullable
    private static IUserDtoRepository userRepository;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin("test1");
        user1.setPasswordHash(HashUtil.salt(propertyService, "test1"));
        entityManager.getTransaction().begin();
        userRepository.add(user1);
        entityManager.getTransaction().commit();
        USER1_ID = user1.getId();

        @NotNull final UserDto user2 = new UserDto();
        user2.setLogin("test2");
        user2.setPasswordHash(HashUtil.salt(propertyService, "test2"));
        entityManager.getTransaction().begin();
        userRepository.add(user2);
        entityManager.getTransaction().commit();
        USER2_ID = user2.getId();

        entityManager.close();
    }

    @Before
    public void initRepository() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);
        sessionList = new ArrayList<>();
        sessionRepository = new SessionDtoRepository(entityManager);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            final boolean condition = i < NUMBER_OF_ENTRIES / 2;
            @NotNull final String userId = condition ? USER1_ID : USER2_ID;
            @NotNull final SessionDto session = new SessionDto(userId, Role.USUAL);
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            sessionList.add(session);
        }
        entityManager.close();
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        entityManager.getTransaction().begin();
        userRepository.removeOneById(USER1_ID);
        userRepository.removeOneById(USER2_ID);
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @After
    public void initClear() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        for (@NotNull final SessionDto session : sessionList) {
            @Nullable final String userId = session.getUserId();
            assertNotNull(userId);
            entityManager.getTransaction().begin();
            sessionRepository.removeOneById(userId, session.getId());
            entityManager.getTransaction().commit();
        }
        sessionList.clear();

        entityManager.close();
    }

    @Test
    public void testAdd() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        final int expectedSize = sessionRepository.getSize().intValue();
        @NotNull final SessionDto session = new SessionDto(USER1_ID, Role.USUAL);
        entityManager.getTransaction().begin();
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
        assertEquals(expectedSize + 1, sessionRepository.getSize().intValue());

        @Nullable final String userId = session.getUserId();
        assertNotNull(userId);
        entityManager.getTransaction().begin();
        sessionRepository.removeOneById(userId, session.getId());
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        for (@NotNull final String userId : userList) {
            entityManager.getTransaction().begin();
            sessionRepository.removeAll(userId);
            entityManager.getTransaction().commit();
        }
        assertEquals(0, sessionRepository.getSize(USER1_ID).intValue());
        assertEquals(0, sessionRepository.getSize(USER2_ID).intValue());

        entityManager.close();
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        final int expectedSize = sessionRepository.getSize().intValue();
        @NotNull final String userId = UUID.randomUUID().toString();
        entityManager.getTransaction().begin();
        sessionRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        assertEquals(expectedSize, sessionRepository.getSize().intValue());

        entityManager.close();
    }

    @Test
    public void testFindByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        for (@NotNull final SessionDto session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            @Nullable final SessionDto actualSession = sessionRepository.findOneById(userId, id);
            assertNotNull(actualSession);
            assertEquals(session.getId(), actualSession.getId());
        }

        entityManager.close();
    }

    @Test
    public void testFindByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        @NotNull final String id = UUID.randomUUID().toString();
        @NotNull final String userId = UUID.randomUUID().toString();
        assertNull(sessionRepository.findOneById(userId, id));

        entityManager.close();
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        assertEquals(HALF_NUMBER_OF_ENTRIES, sessionRepository.getSize(USER1_ID).intValue());
        assertEquals(HALF_NUMBER_OF_ENTRIES, sessionRepository.getSize(USER2_ID).intValue());

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        for (@NotNull final SessionDto session : sessionList) {
            @Nullable final String userId = session.getUserId();
            @NotNull final String id = session.getId();
            entityManager.getTransaction().begin();
            sessionRepository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
            assertNull(sessionRepository.findOneById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        sessionRepository = new SessionDtoRepository(entityManager);

        final int expectedSize = sessionRepository.getSize().intValue();
        @NotNull final String otherId = UUID.randomUUID().toString();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        entityManager.getTransaction().begin();
        sessionRepository.removeOneById(otherUserId, otherId);
        entityManager.getTransaction().commit();
        assertEquals(expectedSize, sessionRepository.getSize().intValue());

        entityManager.close();
    }
*/

}
