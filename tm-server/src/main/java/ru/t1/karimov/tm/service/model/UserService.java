package ru.t1.karimov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.model.IUserRepository;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.model.IProjectService;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.api.service.model.ITaskService;
import ru.t1.karimov.tm.api.service.model.IUserService;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.exception.field.EmailEmptyException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.ExistsEmailException;
import ru.t1.karimov.tm.exception.user.ExistsLoginException;
import ru.t1.karimov.tm.exception.user.RoleEmptyException;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.model.UserRepository;
import ru.t1.karimov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private final IProjectService projectService;

    @NotNull
    @Autowired
    private final ITaskService taskService;

    @NotNull
    @Autowired
    private final ISessionService sessionService;

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(UserRepository.class);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull IUserRepository repository = getRepository();
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull IUserRepository repository = getRepository();
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull IUserRepository repository = getRepository();
            return repository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull IUserRepository repository = getRepository();
            return repository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            @Nullable User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOne(@Nullable final User model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        @NotNull final String userId = model.getId();
        if (!existsById(userId)) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            taskService.removeAll(userId);
            projectService.removeAll(userId);
            sessionService.removeAll(userId);
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            taskService.removeAll();
            projectService.removeAll();
            sessionService.removeAll();
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        removeOne(
                Optional.ofNullable(findByLogin(login))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    public void removeOneByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        removeOne(
                Optional.ofNullable(findByEmail(email))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            @NotNull final User user = Optional.ofNullable(repository.findOneById(id))
                    .orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final String fName = (firstName == null) ? "" : firstName;
        @NotNull final String lName = (lastName == null) ? "" : lastName;
        @NotNull final String mName = (middleName == null) ? "" : middleName;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            @NotNull final User user = Optional.ofNullable(repository.findOneById(id))
                    .orElseThrow(UserNotFoundException::new);
            user.setFirstName(fName);
            user.setLastName(lName);
            user.setMiddleName(mName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = getRepository();
            @NotNull User user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
