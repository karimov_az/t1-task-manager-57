package ru.t1.karimov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.model.ISessionRepository;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.repository.model.SessionRepository;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(SessionRepository.class);
    }

}
