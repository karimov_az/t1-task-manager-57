package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.service.dto.IProjectDtoService;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto> implements IProjectDtoService {

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository() {
        return context.getBean(ProjectDtoRepository.class);
    }

    @NotNull
    @Override
    public ProjectDto changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = getRepository();
            @NotNull final ProjectDto project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDto changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        if (status == null) throw new StatusEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = getRepository();
            @NotNull final ProjectDto project = Optional
                    .ofNullable(repository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return create(userId, name);
        @NotNull final ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setStatus(status);
        @NotNull final ProjectDto result = add(userId, project);
        return result;
    }

    @NotNull
    @Override
    public ProjectDto updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = getRepository();
            @NotNull final ProjectDto project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDto updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository repository = getRepository();
            @NotNull final ProjectDto project = Optional
                    .ofNullable(repository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
