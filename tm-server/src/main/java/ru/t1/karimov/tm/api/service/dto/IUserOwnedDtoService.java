package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedDtoModel> extends IDtoService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<M> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    Long getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeOne(@Nullable String userId, M model) throws Exception;

    void removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void update(@Nullable String userId, M model) throws Exception;

}
