package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.karimov.tm.api.service.dto.ISessionDtoService;
import ru.t1.karimov.tm.dto.model.SessionDto;
import ru.t1.karimov.tm.repository.dto.SessionDtoRepository;

@Service
@AllArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto> implements ISessionDtoService {

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository() {
        return context.getBean(SessionDtoRepository.class);
    }

}
