package ru.t1.karimov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.task.TaskClearRequest;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[TASKS CLEAR]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

}
